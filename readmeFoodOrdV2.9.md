# WHAT2EAT Official Website
## 📄 Description
- ### <u>**Ordering Page of WHAT2EAT Food Brand:**</u> For Customers who want to order food, ask questions, or become a partner of the brand.
- ### <u>**Orders List Page of WHAT2EAT Food Brand:**</u> For the Admin, who manages all orders.
- ### <u>**Sự kiện click chọn loại Pizza sẽ đưa lên LocalStorage:**</u>.
- ### <u>**Sự kiện click vào giỏ hàng sẽ sang trang oder hiện loại pizza đã chọn và các chức năng khác**</u>.
## ✨Feature
+ <u>**Home Page:**</u>
![Homepage](images/01_img_home_page.PNG)
+ <u>**Menu Section:**</u> You can choose pizza type and quantity to add to your cart.
![Menu](images/02_img_menu_section.PNG)
+ <u>**Blog Section:**</u>
![Blog](images/03_img_blog_section.PNG)
+ <u>**Contact Us:**</u>
![ContactUs](images/04_img_contact_us.PNG)
+ <u>**Products List Page:**</u> List of products with pagination, Customers can also filter products that suit their needs.
![ProductsList](images/10_img_products_list_page.PNG)
+ <u>**Cart Page:**</u> You can increase or decrease quantity of each pizza type as well as removing them. Or you can also redeem your voucher code to get discount.
![CartPage](images/05_img_cart_page.PNG)
+ <u>**Check Out Section:**</u> Enter your infomation to make payment and get quick delivery.
![CheckOut](images/06_img_check_out.PNG)
+ <u>**Orders List (for Admin):**</u> Admin can manage all orders in table form.
![OrdersList](images/07_img_orders_list.PNG)
+ <u>**Editing Order (for Admin):**</u> From orders list, Admin can choose a order to view its details as well as editing it.
![Editing](images/08_img_editing_orders.PNG)
+ <u>**Deleting Order (for Admin):**</u> From orders list, Admin can choose a order to remove.
![Editing](images/09_img_deleting_orders.PNG)

## 🧱Technology
+ **Front-end**:
    1. [Bootstrap 4](https://getbootstrap.com/docs/4.0/getting-started/introduction/)
    2. Javascript
    3. [Jquery 3](https://jquery.com/)
    4. AJAX
    5. [Font Awesome 6](https://fontawesome.com/start)
    6. [DataTables](https://datatables.net/manual/installation)